
const companies = [
    { name: "Company One", category: "Finance", start: 1981, end: 2004 },
    { name: "Company Two", category: "Retail", start: 1992, end: 2008 },
    { name: "Company Three", category: "Auto", start: 1999, end: 2007 },
    { name: "Company Four", category: "Retail", start: 1989, end: 2010 },
    { name: "Company Five", category: "Technology", start: 2009, end: 2014 },
    { name: "Company Six", category: "Finance", start: 1987, end: 2010 },
    { name: "Company Seven", category: "Auto", start: 1986, end: 1996 },
    { name: "Company Eight", category: "Technology", start: 2011, end: 2016 },
    { name: "Company Nine", category: "Retail", start: 1981, end: 1989 }
  ];
  
  const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];
  
  const person = {
    name: "Costas",
    address: {
      street: "Lalaland 12"
    }
  };
let text;
  
//Exercise 1: In index.js print the name of each company using forEach
console.log("Ans of Exercise 1: ");
companies.forEach(companyName)

function companyName(companies, index){
    text= index +" : " +companies.name;
    console.log(text);

}
console.log("\n");
//console.log(text);


//Exercise 2: In index.js print the name of each company that started after 1987
console.log("Ans of Exercise 2: ");
const companiesAfter1987 = companies.reduce((acc, curr)=>{
    if(curr.start > 1987){
        acc.push(curr.name);
    }
    return acc;
},[])

console.log(companiesAfter1987);
console.log("\n");
// function check(companies){
//     if(companies.start>1987){
//         ans+=companies.name;
//     }
//     return ans;
// }

//Exercise 3: In index.js get only the companies that have category Retail, increment their start by 1 and append in the DOM a div that has the name, the category, the start and the end in paragraphs elements
console.log("Ans of Exercise 3: ");
const ratailCompanies= companies.reduce((acc, curr)=>{
    if(curr.category==='Retail'){
        curr.start++;
        acc.push(curr);
    }
    return acc;
},[])
console.log(ratailCompanies);
console.log("\n");
//Exercise 4: In index.js sort the companies based on their end date in asceding order
console.log("Ans of Exercise 4: ");
const sortedEndDate=companies.map(companies=>companies.end).sort();

console.log("sortedEndDates : " + sortedEndDate);
console.log("\n");

//Exercise 5: In index.js sort the ages array in desceding order

console.log("Ans of Exercise 5: ");
ages.sort();
ages.reverse();

console.log("Ages: "+ ages);
console.log("\n");

// Exercise 6: In index.js print the sum if you add all the ages using reduce
console.log("Ans of Exercise 6: ");

const sumOfAges= ages.reduce((acc, curr)=>{
    acc+=curr;
    return acc;
},0)
console.log("Sum of All Ages: "+sumOfAges);
console.log("\n");

//Exercise 7: In index.js make an new object that has the properties of name and category same as the companies[0] and a method print that prints out the name, use object destructuring and ES6 JS
console.log("Ans of Exercise 7: ")
const company = {name: "Company One", category: "Finance"};

function print(company){
    const{name, category}=company;
    console.log("Name : "+name)
}
print(company);
console.log("\n")

//Exercise 8: In index.js create a funcion that takes an unkown number of arguments that are numbers and return their sum;

console.log("Ans of Exercise 8: ");
function sum(...nums) {
    let total = 0;
    for (const num of nums) {
      total += num;
    }
    return total;
  }
  
  console.log(sum(1, 2, 3));
  console.log("\n");

  //Exercise 9: In index.js make a function that takes an unkown number of arguments of any type and adds them in an array and returns the array, if the argument is an array it should add it's values to the array that will be returned by the function
  console.log("Ans of Exercise 9: ");

  const arr=[1, [2, 5,[10,15]], 6, [7, 8]];
  //Inbuilt function to flatten array
  console.log(arr.flat());

  //Recursive logic using foreach to flatten the array and also nested array (Matrix)

  function flattenArray(arr){
    const ans=[];
    arr.forEach((item)=>{
    if(Array.isArray(item)){
        ans.push(...flattenArray(item));
        
    }else{
    ans.push(item);
}
  });
return ans;
}

console.log(flattenArray(arr));
console.log("\n")

//Exercise 10:index.js distructure the property street in a variable named street from the object person

console.log("Ans of Exercise 10: ");
// const {name, address}=person;
// const {street}= address;
// console.log(street);



const {street}= person.address;
console.log(street);
console.log("\n");

//Exercise 11: In index.js write a function that everytime you call it, it returns a number that increments starting from 0
console.log("Ans of Exercise 11: ");
let value=0;
function increament(){
    return ++value;
}
console.log(increament());
console.log(increament());

console.log("\n");

//Exercise 12: In index.js create a function that distructures the query parameters of a url and adds them in an object as key value pairs and then returns the object
//I did not understand this question and not able to solve after understanding also so need to take help from internet
console.log("Ans of Exercise 12: ");

const url = require('url');

function parseUrlQueryParams(urlString) {
  const parsedUrl = new URL(urlString);
  const queryParams = {};

  parsedUrl.searchParams.forEach((value, key) => {
    queryParams[key] = value;
  });

  return queryParams;
}

const urlExample = 'https://example.com/path?name=John&age=30';
const parsedParams = parseUrlQueryParams(urlExample);

console.log(parsedParams);
console.log("\n");